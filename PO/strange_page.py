'This file contains PO desing'

from webelements import elements
from webelements.locators import *
from settings.setup_variables import global_wait
import re
import time


class stranger_page(object):
    def __init__(self, myDriver):
        self.driver = myDriver
        self.items = []
        self.image_input = elements.input_text(self.driver, stranger_page_locators['item_details']['image_input'])
        self.input_text = elements.input_text(self.driver, stranger_page_locators['item_details']['input_text'])
        self.create_button = elements.button(self.driver, stranger_page_locators['item_details']['create_button'])
        self.cancel_button = elements.button(self.driver, stranger_page_locators['item_details']['cancel_button'])
        self.update_button = elements.button(self.driver, stranger_page_locators['item_details']['update_button'])
        self.lightbox = elements.confirm_light_box(self.driver, stranger_page_locators)

    @property
    def displayed_items(self):
        self.displayed_items_text = elements.text(self.driver, stranger_page_locators['displayed_items'])
        return int(re.findall(r'\b\d+\b', self.displayed_items_text.get_text)[0])

    def innit_items_div(self):
        for item in range(self.displayed_items):
            case = elements.item_div(self.driver, item)
            case.innit_div_elements()
            self.items.append(case)

    def wait_item_counter(self, old_value):
        wait = 0
        while True:
            if self.displayed_items == old_value and wait < global_wait:
                time.sleep(1)
                wait +=1
            else:
                break
