import os
import time
import unittest
from test_cases import stranger_page_TCs
from Test_suite import HTMLTestRunner

direct = os.getcwd()



class TestSuite(unittest.TestCase):

    def test_suite_regression(self):
        smoke_test = unittest.TestSuite()
        smoke_test.addTests([
            unittest.defaultTestLoader.loadTestsFromTestCase(stranger_page_TCs.happy_paths),
            unittest.defaultTestLoader.loadTestsFromTestCase(stranger_page_TCs.unhappy_paths),

        ])

        outfile = open("..\Test_suite\outputs\%s_Test_Suite.html" %time.strftime('%Y%m%d_%H%M%S'), "w", encoding="utf8")

        runner1 = HTMLTestRunner.HTMLTestRunner(
                stream=outfile,
                title='Test Report',
                description='Smoke Tests'
        )

        runner1.run(smoke_test)

if __name__ == '__main__':
    unittest.main()