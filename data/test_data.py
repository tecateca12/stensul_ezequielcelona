''' this file contains data for test purposes'''


from random import choice
data_sets = {
    'happy_path' :{
        'description_300' : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.',
        'default_create' : 'This is an automated test, enjoy it, it\'s an edited and happy path',
        'default_edit' :  'This is an automated test, enjoy it, it\'s an edited and happy path'
    },
    'negative_paths' : {
        'description_over_300': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.1'
    }
}

images = {
    'happy_path' : {
        'jpg' : '{}\\Uploads\\happy_path\\320x320.jpg',
        'png' : '{}\\Uploads\\happy_path\\320x320.png',
        'gif' : '{}\\Uploads\\happy_path\\320x320.gif',
        'JPG': '{}\\Uploads\\happy_path\\320x320(1).JPG',
        'PNG': '{}\\Uploads\\happy_path\\320x320(1).PNG',
        'GIF': '{}\\Uploads\\happy_path\\320x320(1).GIF'
    },
    'negative_path': {
        'invalid_format' : {
            'pdf': '{}\\Uploads\\negative_path\\320x320.pdf',
            'zip': '{}\\Uploads\\negative_path\\zip_file.zip',
            'txt': '{}\\Uploads\\negative_path\\txt_file.txt',
            'psd': '{}\\Uploads\\negative_path\\psd_file.psd',
            'bmp': '{}\\Uploads\\negative_path\\bmp_file.bmp'

        },
        'invalid_dimensions' : {
            '321x320': '{}\\Uploads\\negative_path\\321x320.jpg',
            '320x321': '{}\\Uploads\\negative_path\\320x321.jpg',
            '1x1': '{}\\Uploads\\negative_path\\1x1.jpg'
        }
    }
}

