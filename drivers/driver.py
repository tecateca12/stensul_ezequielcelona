'this file contains drivers to be selected after run tests'
from selenium import webdriver

class chrome(object):
    def __init__(self):
        self.driver = webdriver.Chrome('../drivers/chromedriver.exe')
        self.driver.maximize_window()

class firefox(object):
    def __init__(self):
        self.driver = webdriver.Firefox(executable_path = '../drivers/geckodriver.exe')
        self.driver.maximize_window()
