'this file contains methods to initialize the test variables'

import json
from drivers.driver import *
#####################################################################################################

def return_json(file_name):
    with open('{}'.format(file_name), 'r') as f:
        env = json.load(f)
    return env

#####################################################################################################

urls = return_json('../settings/config.json')['urls']
def get_base_url():
    return urls[return_json('../settings/config.json')['environment']]
base_url = get_base_url()

#####################################################################################################

def get_global_wait():
    return return_json('../settings/config.json')['global_wait']

global_wait = get_global_wait()

#####################################################################################################

def get_driver():
    if return_json('../settings/config.json')['driver'] == 'chrome':
        return chrome
    elif return_json('../settings/config.json')['driver'] == 'firefox':
        return firefox

driver = get_driver()