'this file contains test cases'

import unittest
import os
from settings.setup_variables import base_url, driver, global_wait
from PO.strange_page import stranger_page
from data.test_data import data_sets, images
from random import choice
import time

class happy_paths(unittest.TestCase):
    def setUp(self):
        self.driver = driver().driver
        self.stranger_page = stranger_page(self.driver)
        self.driver.implicitly_wait(global_wait)
        self.driver.get(base_url)
        self.displayed_items = self.stranger_page.displayed_items
        self.working_directory = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        self.image_for_test = choice(list(images['happy_path'].items()))[1]

    def test01_create_item_happy_path(self):
        self.stranger_page.image_input.fill_input(self.image_for_test.format(self.working_directory))
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_create'])
        self.assertTrue(self.stranger_page.create_button.get_state)
        self.stranger_page.create_button.click_button()
        self.stranger_page.wait_item_counter(self.displayed_items)
        try:
            self.assertEqual(self.stranger_page.displayed_items, self.displayed_items + 1)
        except:
            raise Exception('Item wasn\'t created correctly.')
        self.displayed_items += 1
        try:
            self.assertEqual(len(self.stranger_page.input_text.get_value), 0)
        except:
            raise Exception ('Item on page was created correctly, but text-area was not cleared.')
        try:
            self.assertEqual(len(self.stranger_page.image_input.get_value), 0)
        except:
            raise Exception ('Item on page was created correctly, but image input was not cleared.')
    def test02_edit_item_description(self):
        self.stranger_page.innit_items_div()
        random_item = choice(self.stranger_page.items)
        random_item.edit_button.click_button()
        self.stranger_page.input_text.fill_input(' Edited item')
        self.stranger_page.update_button.click_button()
        try:
            self.assertTrue(' Edited item' in random_item.description_text.get_text)
        except:
            raise Exception('Description wasn\'t updated correctly.')

    def test03_edit_image(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[-1]
        item.edit_button.click_button()
        self.stranger_page.image_input.fill_input(self.image_for_test.format(self.working_directory))
        self.stranger_page.update_button.click_button()
        try:
            self.assertTrue(self.image_for_test in item.item_image.get_path)
        except:
            raise Exception('Image wasn\'t updated correctly.')

    def test03_edit_full_happy_path(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[-1]
        item.edit_button.click_button()
        self.stranger_page.image_input.fill_input(self.image_for_test.format(self.working_directory))
        self.stranger_page.input_text.clear_input()
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_edit'])
        self.stranger_page.update_button.click_button()
        try:
            self.assertEqual(data_sets['happy_path']['default_edit'] , item.description_text.get_text.strip())
        except:
            raise Exception('Image wasn\'t updated correctly.')
        try:
            self.assertTrue(self.image_for_test in item.item_image.get_path)
        except:
            raise Exception('Image wasn\'t updated correctly.')

    def test04_edit_cancel_path(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[1]
        original_description = item.description_text.get_text
        original_image = item.item_image.get_path

        item.edit_button.click_button()
        self.stranger_page.image_input.fill_input(self.image_for_test.format(self.working_directory))
        self.stranger_page.input_text.clear_input()
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_edit'])
        self.stranger_page.cancel_button.click_button()
        self.assertEqual(original_description, item.description_text.get_text)
        self.assertEqual(original_image, item.item_image.get_path)

    def test05_delete_item(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[-1]
        item.delete_button.click_button()
        self.stranger_page.lightbox.confirm_delete_button.click_button()

        self.stranger_page.wait_item_counter(self.displayed_items)
        try:
            self.assertEqual(self.stranger_page.displayed_items, self.displayed_items - 1)
        except:
            raise Exception('Items on page wasn\'t deleted correctly.')
        self.displayed_items -= 1

    def test06_delete_while_editing(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[-1]
        item.edit_button.click_button()
        item.delete_button.click_button()
        self.stranger_page.lightbox.confirm_delete_button.click_button()
        self.stranger_page.wait_item_counter(self.displayed_items)
        try:
            self.assertEqual(self.stranger_page.displayed_items, self.displayed_items - 1)
        except:
            raise Exception('Item on page wasn\'t deleted correctly.')
        self.displayed_items -= 1

        try:
            self.assertFalse(self.stranger_page.update_button.is_displayed)
        except:
            raise Exception ('Item was deleted correctly, but item details remains on "edit mode"')

    def test06_delete_item_cancel(self):
        self.stranger_page.innit_items_div()
        item = self.stranger_page.items[-1]
        item.delete_button.click_button()
        self.stranger_page.lightbox.cancel_delete_button.click_button()

        self.stranger_page.wait_item_counter(self.displayed_items)
        try:
            self.assertEqual(self.stranger_page.displayed_items, self.displayed_items)
        except:
            raise Exception('Item on page was deleted.')

    # This test case will Check if exist in the list the item with text “Creators: Matt Duffer, Ross Duffer”
    # Test will fail if doesn't
    def test07_detect_text_on_item(self):
        self.stranger_page.innit_items_div()
        occurrences = 0
        for item in self.stranger_page.items:
            if 'Creators: Matt Duffer, Ross Duffer' in item.description_text.get_text:
                occurrences +=1
            else:
                pass
        self.assertTrue(occurrences > 0)
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()

class unhappy_paths(unittest.TestCase):
    def setUp(self):
        self.driver = driver().driver
        self.stranger_page = stranger_page(self.driver)
        self.driver.implicitly_wait(global_wait)
        self.driver.get(base_url)
        self.displayed_items = self.stranger_page.displayed_items
        self.working_directory = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

    def test01_create_over_300char(self):
        self.stranger_page.image_input.fill_input(images['happy_path']['png'].format(self.working_directory))
        self.stranger_page.input_text.fill_input(data_sets['negative_paths']['description_over_300'])
        self.assertFalse(self.stranger_page.create_button.get_state)
        try:
            self.stranger_page.create_button.click_button()
        except:
            pass
        self.stranger_page.wait_item_counter(self.displayed_items)
        try:
            self.assertEqual(self.stranger_page.displayed_items, self.displayed_items)
        except:
            raise Exception('Items on page wasn\'t updated correctly.')

    def test02_create_wrong_file_format(self):
        self.stranger_page.image_input.fill_input(images['negative_path']['invalid_format']['txt'].format(self.working_directory))
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_create'])
        self.assertFalse(self.stranger_page.create_button.get_state)

    def test03_create_wrong_file_dimensions(self):
        self.stranger_page.image_input.fill_input(images['negative_path']['invalid_dimensions']['321x320'].format(self.working_directory))
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_create'])
        self.assertFalse(self.stranger_page.create_button.get_state)

    def test04_create_required_fields(self):
        self.assertFalse(self.stranger_page.create_button.get_state)
        self.stranger_page.image_input.fill_input(images['happy_path']['jpg'].format(self.working_directory))
        self.assertFalse(self.stranger_page.create_button.get_state)
        ####################################################
        self.driver.refresh()
        self.assertFalse(self.stranger_page.create_button.get_state)
        self.stranger_page.input_text.fill_input(data_sets['happy_path']['default_create'])
        self.assertFalse(self.stranger_page.create_button.get_state)

    def test05_edit_over_300char(self):
        self.stranger_page.innit_items_div()
        random_item = choice(self.stranger_page.items)
        random_item.edit_button.click_button()
        self.assertTrue(self.stranger_page.update_button.get_state)
        self.stranger_page.input_text.clear_input()
        self.assertFalse(self.stranger_page.update_button.get_state)
        self.stranger_page.input_text.fill_input(data_sets['negative_paths']['description_over_300'])
        self.assertFalse(self.stranger_page.update_button.get_state)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()