'''design patter'''
from selenium.webdriver.common.action_chains import ActionChains
from webelements.waits import waits
from webelements.locators import *
from selenium.webdriver.support import expected_conditions as EC
import unittest

class input_text(object):
    def __init__(self, myDriver, locator):
        # Driver
        self.driver = (myDriver)
        self.locator = locator
        self.wait = waits(self.driver)

    def scroll_to_view(self):
        self.wait.implicitly_wait()
        element = self.driver.find_element(*self.locator)
        self.driver.execute_script("return arguments[0].scrollIntoView(true);",element)
        self.driver.execute_script("window.scrollBy(0, -500);")

    def fill_input(self, text_to_fill):
        self.scroll_to_view()
        self.wait.until(EC.element_to_be_clickable(self.locator))
        self.driver.find_element(*self.locator).send_keys(text_to_fill)

    def clear_input(self):
        self.scroll_to_view()
        self.wait.until(EC.element_to_be_clickable(self.locator))
        self.driver.find_element(*self.locator).clear()

    @property
    def get_value(self):
        self.scroll_to_view()
        self.wait.until(EC.presence_of_element_located(self.locator))
        return self.driver.find_element(*self.locator).get_attribute('value')

class button(object):
    def __init__(self, myDriver, locator):
        # Driver
        self.driver = (myDriver)
        self.locator = locator
        self.wait = waits(self.driver)

    def scroll_to_view(self):
        self.wait.until(EC.presence_of_element_located(self.locator))
        element = self.driver.find_element(*self.locator)
        self.driver.execute_script("return arguments[0].scrollIntoView(true);",element)
        self.driver.execute_script("window.scrollBy(0, -500);")

    def click_button(self):
        self.scroll_to_view()
        self.wait.until(EC.element_to_be_clickable(self.locator))
        self.driver.find_element(*self.locator).click()


    def hover_over(self):
        self.scroll_to_view()
        self.wait.until(EC.element_to_be_clickable(self.locator))
        element = self.driver.find_element(*self.locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    # This function will get the state enabled of disabled of create button,
    # Will return True if it's enabled.
    @property
    def get_state(self):
        self.scroll_to_view()
        self.wait.until(EC.presence_of_element_located(self.locator))
        if self.driver.find_element(*self.locator).get_attribute('disabled') == 'true':
            return False
        else:
            return True
    @property
    def is_displayed(self):
        try:
            element = self.driver.find_element(*self.locator)
        except:
            return False
        return(element.is_displayed())


class text(object):
    def __init__(self, myDriver, locator):
        self.driver = (myDriver)
        self.locator = locator
        self.tc = unittest.TestCase('__init__')
        self.wait = waits(self.driver)

    def scroll_to_view(self):
        self.wait.implicitly_wait()
        error_message = self.driver.find_element(*self.locator)
        self.driver.execute_script("return arguments[0].scrollIntoView(true);", error_message)
        self.driver.execute_script("window.scrollBy(0, -500);")

    @property
    def get_text(self):
        self.scroll_to_view()
        self.wait.until(EC.presence_of_element_located(self.locator))
        return self.driver.find_element(*self.locator).text

    def assert_text(self, expected_text):
        self.scroll_to_view()
        self.wait.until(EC.presence_of_element_located(self.locator))
        self.tc.assertEqual(self.get_text(), expected_text)

class image(object):
    def __init__(self, myDriver, locator):
        # Driver
        self.driver = (myDriver)
        self.locator = locator
        self.wait = waits(self.driver)

    @property
    def get_path(self):
        self.wait.until(EC.presence_of_element_located(self.locator))
        return self.driver.find_element(*self.locator).get_attribute('src')

class item_div(object):
    def __init__(self, myDriver, index_position):
        self.driver = (myDriver)
        self.index_position = index_position
        self.locators = stranger_item_locators(self.index_position)
        self.tc = unittest.TestCase('__init__')
        self.wait = waits(self.driver)

    def innit_div_elements(self):
        self.item_image = image(self.driver, self.locators['item_image'])
        self.description_text = text(self.driver,self.locators['description_text'])
        self.delete_button = button(self.driver,self.locators['delete_button'])
        self.edit_button = button(self.driver,self.locators['edit_button'])

class confirm_light_box(object):
    def __init__(self, myDriver, locators):
        self.locators = locators
        self.driver = (myDriver)
        self.tc = unittest.TestCase('__init__')
        self.wait = waits(self.driver)
        self.confirm_delete_button = button(self.driver, self.locators['lightbox']['confirm_delete_button'])
        self.cancel_delete_button = button(self.driver,self.locators['lightbox']['cancel_delete_button'])


