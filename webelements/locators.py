'locators for elements'

from selenium.webdriver.common.by import By

# this is a function that will provide us dynamics locators for each sub item within main div item.
def stranger_item_locators(index_position):
    index_position +=1
    stranger_item_locators = {
        'item_image' : (By.XPATH, "(*//li[@ng-repeat='item in items']//img)[{}]".format(index_position)),
        'main_div' : (By.XPATH, "(*//li[@ng-repeat='item in items'])[{}]".format(index_position)),
        'description_text': (By.XPATH, "(//li[@ng-repeat='item in items']//p)[{}]".format(index_position)),
        'delete_button': ((By.XPATH, "(*//li[@ng-repeat='item in items']//button[contains(text(),'Delete')])[{}]".format(index_position))),
        'edit_button' : ((By.XPATH, "(*//li[@ng-repeat='item in items']//button[contains(text(),'Edit')])[{}]".format(index_position)))
    }
    return stranger_item_locators

stranger_page_locators = {
    "displayed_items": (By.TAG_NAME, 'h1'),
    "item_details": {
        'image_input': (By.ID, 'inputImage'),
        "input_text": (By.TAG_NAME, 'textarea'),
        'create_button': (By.XPATH, "*//div[@ng-if='!strangerlist.currentItem']/button[contains(text(),'Create')]"),
        'cancel_button': (By.XPATH, "*//div[@ng-if='strangerlist.currentItem']/button[contains(text(),'Cancel')]"),
        'update_button' : (By.XPATH, "*//div[@ng-if='strangerlist.currentItem']/button[contains(text(),'Update')]")
    },
    "lightbox" : {
        'confirm_delete_button' : (By.XPATH, "//div[@class = 'modal-dialog']//button[@ng-click='submit()']"),
        'cancel_delete_button' : (By.XPATH, "//div[@class = 'modal-dialog']//button[@ng-click='cancel()']")
    }

}
# Global locators for stranger page

