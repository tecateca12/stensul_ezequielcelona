'global waits'
from settings.setup_variables import global_wait
from selenium.webdriver.support.ui import WebDriverWait

class waits(WebDriverWait):
    def __init__(self, myDriver):
        self.driver = myDriver
        self.time = global_wait
        super().__init__(self.driver,self.time)

    def implicitly_wait(self):
        self.driver.implicitly_wait(self.time)


